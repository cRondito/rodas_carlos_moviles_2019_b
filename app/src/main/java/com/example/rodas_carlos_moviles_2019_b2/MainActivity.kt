package com.example.rodas_carlos_moviles_2019_b2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var game_title: TextView
    private lateinit var game_timer: TextView
    private lateinit var game_tohit: TextView
    private lateinit var game_button: Button
    private lateinit var game_points: TextView

    private lateinit var countDownTimer: CountDownTimer
    private var countDownInterval: Long = 1000

    private var timeLeft = 10
    private var gameScore = 0

    private var n = (1..9).shuffled().first()

    private var isGameStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        n = (1..9).shuffled().first()

        game_title = findViewById(R.id.game_title)
        game_timer = findViewById(R.id.game_timer)
            game_timer.text = getString(R.string.game_timer_text, timeLeft)
        game_tohit = findViewById(R.id.game_tohit)
            game_tohit.text = getString(R.string.game_tohit_text, n)
        game_button = findViewById(R.id.game_button)
        game_points = findViewById(R.id.game_points)
            game_points.text = getString(R.string.game_points_text, gameScore)

        resetGame()

        game_button.setOnClickListener {
            if(!isGameStarted){
                startGame()
            }

            //gameScore ++
            //game_points.text = getString(R.string.game_points_text, gameScore)
            if (timeLeft == n){
                gameScore += 100
                game_points.text = getString(R.string.game_points_text, gameScore)
            }

            val aux1 = n+1
            val aux2 = n-1

            if (timeLeft == aux1 || timeLeft == aux2) {
                    gameScore += 50
                game_points.text = getString(R.string.game_points_text, gameScore)
                }
        }

    }


    private fun startGame(){
        countDownTimer.start()
        isGameStarted = true
    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_LONG).show()
        resetGame()
    }

    private fun resetGame(){
        gameScore = 0
        game_points.text = getString(R.string.game_points_text, gameScore)

        timeLeft = 10
        game_timer.text = getString(R.string.game_timer_text, timeLeft)

        configCountdownTimer()

        n = (1..9).shuffled().first()
        game_tohit.text = getString(R.string.game_tohit_text, n)

        isGameStarted = false
    }

    private fun configCountdownTimer(){
        countDownTimer = object : CountDownTimer((timeLeft * 1000).toLong(), countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                game_timer.text = getString(R.string.game_timer_text, timeLeft)
            }
            override fun onFinish() {
                endGame()
            }
        }
    }
}
